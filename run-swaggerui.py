#!/usr/bin/env python3

from heaserver.volume import service
from heaserver.service import swaggerui
from integrationtests.heaserver.volumeintegrationtest.testcase import db_store
import logging

logging.basicConfig(level=logging.DEBUG)

if __name__ == '__main__':
    swaggerui.run('heaserver-volumes', db_store, service, [('/volumes/{id}', service.get_volume)])
